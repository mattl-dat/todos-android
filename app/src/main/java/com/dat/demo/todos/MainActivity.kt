package com.dat.demo.todos

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception

class MainActivity : AppCompatActivity(), TodoActor {

    private val app
        get() = application as TodoApplication

    private val adapter
        get() = recycler_view.adapter as TodoListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view.apply {
            setHasFixedSize(true)
            adapter = TodoListAdapter(todoActor = this@MainActivity)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    suspend fun refreshTodos() = coroutineScope {
        try {
            adapter.todos = app.todoGateway.getTodos()
        } catch (e: Exception) {
            Log.e("MainActivity", "Unable to retrieve TODOs", e)
            Toast.makeText(this@MainActivity, "Unable to retrieve TODOs", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onResume() {
        super.onResume()

        lifecycleScope.launch {
            refreshTodos()
        }
    }

    override fun toggleCompleted(id: String) {
        lifecycleScope.launch {
            try {
                app.todoGateway.toggleTodo(id)
            } catch (e: Exception) {
                Log.e("MainActivity", "Error toggling TODO", e)
                Toast.makeText(this@MainActivity, "Error toggling TODO", Toast.LENGTH_SHORT)
                    .show()
            }
            refreshTodos()
        }
    }
}
