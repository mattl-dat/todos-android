package com.dat.demo.todos

import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.ApolloQueryCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.dat.demo.graphql.AllTodosQuery
import com.dat.demo.graphql.ToggleTodoMutation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class TodoGateway(
    private val client: ApolloClient
) {

    suspend fun <T> ApolloCall<T>.execute(): Response<T> = suspendCoroutine { cont ->
        enqueue(object : ApolloCall.Callback<T>() {
            override fun onFailure(e: ApolloException) {
                cont.resumeWithException(e)
            }

            override fun onResponse(response: Response<T>) {
                cont.resume(response)
            }
        })
    }

    suspend fun getTodos(): List<AllTodosQuery.Todo> = withContext(Dispatchers.IO) {
        client.query(AllTodosQuery())
            .execute()
            .data()
            ?.todos
            ?.filterNotNull()
            ?: emptyList()
    }

    suspend fun toggleTodo(id: String) {
        withContext(Dispatchers.IO) {
            client.mutate(ToggleTodoMutation(id)).execute()
        }
    }

}