package com.dat.demo.todos

import android.app.Application
import com.apollographql.apollo.ApolloClient

class TodoApplication: Application() {

    lateinit var todoGateway: TodoGateway

    override fun onCreate() {
        super.onCreate()

        val client = ApolloClient.builder()
            .serverUrl(BuildConfig.SERVER_URL)
            .build()

        todoGateway = TodoGateway(client)
    }
}