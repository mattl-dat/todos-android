package com.dat.demo.todos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dat.demo.graphql.AllTodosQuery
import kotlinx.android.synthetic.main.todo_line_item.view.*
import kotlin.properties.Delegates

class TodoListAdapter(
    private val todoActor: TodoActor
) : RecyclerView.Adapter<TodoViewHolder>() {

    var todos: List<AllTodosQuery.Todo> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.todo_line_item, parent, false)
        return TodoViewHolder(view, todoActor)
    }

    override fun getItemCount(): Int = todos.size

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = todos[position]
        holder.bind(todo)
    }
}

class TodoViewHolder(
    itemView: View,
    private val todoActor: TodoActor
) :
    RecyclerView.ViewHolder(itemView) {

    fun bind(todo: AllTodosQuery.Todo) = with(itemView) {
        completed_image_view.visibility = if (todo.completed == true) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
        id_text_view.text = todo.id
        title_text_view.text = todo.title

        val id = todo.id
        if (id != null) {
            itemView.setOnClickListener { todoActor.toggleCompleted(id) }
        } else {
            itemView.setOnClickListener(null)
        }
    }

}