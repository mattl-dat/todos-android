package com.dat.demo.todos

interface TodoActor {

    fun toggleCompleted(id: String)

}