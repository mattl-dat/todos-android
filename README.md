# GraphQL Proof-of-Concept

This application demonstrates how an Android app may utilize a GraphQL endpoint using [Apollo](https://github.com/apollographql/apollo-android).

See the [GraphQL PoC page](https://transcore.jira.com/wiki/spaces/DO1/pages/672825955/GraphQL) for more details and discoveries around GraphQL.

## Setup

To run the TODO server locally, perform the following commands:

```
$ git clone https://github.com/igorlima/todo-mongo-graphql-server.git
$ cd todo-mongo-graphql-server
$ npm install
$ npm start
```

You can read more about this service here: https://www.compose.com/articles/using-graphql-with-mongodb/